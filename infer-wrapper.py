#!/usr/bin/env python3
# This file is part of frama-c-sv,
# a wrapper around Frama-C that makes it possible
# to execute Frama-C as part of the Competition on Software Verification:
# https://gitlab.com/sosy-lab/software/frama-c-svcomp
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from pathlib import Path
import subprocess
import sys
import argparse
import re
from typing import List, Tuple

__version__ = "sv-comp-22"
global_count = 1


def preprocess_program(text: str) -> Tuple[str, bool]:
    match_assert_definition = re.compile(r"\s*void\s*__VERIFIER_assert\s*\(.+\).*")
    bracket_count = 0
    closing_bracket_index = 0
    assert_definition_code = []
    found_at_least_one_bracket = False
    found_definition = False
    for line in text.splitlines():
        assert_definition = match_assert_definition.match(line)
        found_definition = bool(assert_definition) or found_definition
        if found_definition or bracket_count > 0:
            for (index, character) in enumerate(line):
                if character == "{":
                    bracket_count += 1
                    found_at_least_one_bracket = True
                if character == "}":
                    bracket_count -= 1
                    closing_bracket_index = index
            if bracket_count > 0 or (
                bracket_count == 0
                and found_definition
                and not found_at_least_one_bracket
            ):
                assert_definition_code.append(line)
            elif bracket_count == 0 and found_at_least_one_bracket:
                assert_definition_code.append(line[:closing_bracket_index])
                break
    if assert_definition_code:
        return (
            text.replace(
                "\n".join(assert_definition_code), " ".join(assert_definition_code)
            ),
            True,
        )
    return text, False


def inline_error(text: str, contains_assert=False) -> str:
    # pylint: disable=W0603
    global global_count
    text = text.splitlines()
    new_content = []
    match_reach_error = re.compile((r".*(reach_error\s*\(\s*\)\s*;).*"))
    match_verifier_assert = re.compile(r".*(__VERIFIER_assert\s*\((.*?)\)\s*;).*")
    for line in text:
        reach_error = match_reach_error.match(line)
        verifier_assert = match_verifier_assert.match(line)
        if re.match(r"\s*void\s*__VERIFIER_assert\s*\(", line):
            new_content.append(line)
        elif reach_error:
            new_content.insert(0, f"int __reach_error_{global_count} = 0x7fffffff;")
            new_content.append(
                line.replace(
                    reach_error[1], f"__reach_error_{global_count} = 0x7fffffff + 1;"
                )
            )
            global_count += 1
        elif verifier_assert and contains_assert:
            new_content.insert(0, f"int __reach_error_{global_count} = 0x7fffffff;")
            new_content.append(
                line.replace(
                    verifier_assert[1],
                    f"if ({verifier_assert[2]}) __reach_error_{global_count} = 0x7fffffff + 1;",
                )
            )
            global_count += 1
        else:
            new_content.append(line)
    return "\n".join(new_content)


def create_arg_parser():
    parser = argparse.ArgumentParser(
        description="Wrapper around Infer for use in SV-COMP."
    )

    parser.add_argument("--property", required=True)
    parser.add_argument("--program", required=True)
    parser.add_argument("--datamodel", default="ILP32")
    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument(
        "args",
        nargs="?",
        metavar="ARG",
        help='command line to run (prefix with "--" '
        + "to ensure all arguments are treated correctly)",
    )

    return parser


def read_file(file_name) -> str:
    with open(file_name, "r", encoding="UTF-8") as inp:
        return inp.read()


def store_file(path, content):
    with open(path, "w", encoding="UTF-8") as out:
        out.write(content)


def build_infer_cmd(analysis_property, program_path: str, data_model: str) -> List[str]:
    options = [
        "--no-default-checkers",
        "--biabduction-unsafe-malloc",
        "--continue-analysis",
        "--uninit",
    ] + analysis_property.get_options()
    machine = "-m32" if data_model == "ILP32" else "-m64"
    infer_bin = Path(__file__).parent / "lib" / "infer" / "infer" / "bin" / "infer"
    return [str(infer_bin)] + options + ["--", "gcc", machine, "-c", program_path]


def run_cmd(shlex_command) -> Tuple[str, str]:
    print("Executing command:", " ".join(shlex_command))
    process = subprocess.run(shlex_command, capture_output=True, text=True, check=False)
    return process.stdout, process.stderr


# pylint: disable=R0914
def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    # parse arguments
    parser = create_arg_parser()
    args = parser.parse_args(argv)

    # create property
    property_text = read_file(args.property)
    prp = Property.from_text(property_text)
    print("Use property", prp)

    # read machine model
    data_model = args.datamodel
    print("Use data model", data_model)

    # read program
    program = args.program
    content = (
        read_file(program)
        if not isinstance(prp, UnreachProperty)
        else inline_error(*preprocess_program(read_file(program)))
    )
    tmp_file = "tmp" + program[-2:]
    store_file(tmp_file, content)

    # run tool
    infer_command = build_infer_cmd(prp, tmp_file, data_model)
    analysis_result, analysis_error = run_cmd(infer_command)
    print(analysis_result)
    print(analysis_error)
    result, witness_type = prp.parse_tool_output(analysis_result + analysis_error)

    if result == "true" and Property.found_any_issue(analysis_result + analysis_error):
        print("Looked-for property violation not found, but other violations claimed.")
        result = "unknown"

    # write witness
    if result not in ("error", "unknown"):
        if witness_type == Property.CORRECTNESS_WITNESS:
            witness_creation = witnesses.create_correctness_witness
        elif witness_type == Property.VIOLATION_WITNESS:
            witness_creation = witnesses.create_violation_witness
        else:
            raise ValueError(f"Unknown witness type: {witness_type}")
        architecture = "32bit" if data_model == "ILP32" else "64bit"
        witness = witness_creation(
            Path(args.program),
            Path(args.property),
            producer=__version__,
            architecture=architecture,
        )
        store_file("witness.graphml", witness)

    print(f"Result:{result}")


sys.path.insert(0, Path(__file__).parent)
import witnesses
from property import Property, UnreachProperty

# lib folder of the infer executeable has to be placed next to this script
if __name__ == "__main__":
    sys.exit(main())
