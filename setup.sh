#!/bin/bash

# This file is part of infer-sv,
# the SV-COMP wrapper for Facebook Infer (https://fbinfer.com/).
# https://gitlab.com/sosy-lab/software/infer-sv
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -e

INFER_BIN=lib/infer/infer/bin/infer
if [ -e "$INFER_BIN" ]; then
  echo "File $INFER_BIN already exists. Assuming infer-installation already done."
  exit 2
fi
DIR=$(mktemp -d)
wget https://github.com/facebook/infer/releases/download/v1.1.0/infer-linux64-v1.1.0.tar.xz -O "$DIR"/infer.tar.xz
tar xf "$DIR"/infer.tar.xz -C "$DIR"/
mv "$DIR"/*/lib/* $(dirname "$0")/lib/
rm -r "$DIR"
echo "Infer installation done."

