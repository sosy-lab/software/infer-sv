<!--
This file is part of infer-sv,
the SV-COMP wrapper for Facebook Infer (https://fbinfer.com/).
https://gitlab.com/sosy-lab/software/infer-sv

SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Infer - SV Comp

Infer is a static analyzer from Facebook (https://fbinfer.com/).
This project implements a wrapper for infer to make it compatible 
with the SV-COMP in- and output formats. 

## Setup

Run `./setup.sh` after cloning to download and extract infer
in the expected directory. The `lib` directory should now be placed next to `infer-wrapper.py`.

## Requirements

* Requires python 3.6 or higher


## Raw Usage

This section describes command lines to use the native infer binary for SV-COMP.
Infer indicates violated properties with an error code and the number of times
the property represented by this error code is violated.
Example:
```bash
Found 8 issues (console output truncated to 5, see './path' for the full list)
                Issue Type(ISSUED_TYPE_ID): #
        Null Dereference(NULL_DEREFERENCE): 5
  Nullptr Dereference(NULLPTR_DEREFERENCE): 2
                  Memory Leak(MEMORY_LEAK): 1
```
If no violations are found, infer reports: `No issues found`.

### unreach-call.prp

To check reachability of reach_error, run:

```bash
infer run --annotation-reachability-only --annotation-reachability-cxx "$(cat specs/unreach-call.json)" -- gcc -c PROGRAM.c
```

### valid-memsafety.prp

To check for memory safety, run:

```bash
infer run --no-default-checkers --biabduction --pulse -- gcc -c PROGRAM.c
```

Note that `pulse` is a not yet finished re-implementation of `biabduction`.

### no-overflow.prp

To check if overflows may occur, run:

```bash
infer run --bufferoverrun-only -- gcc -c PROGRAM.c
```

## Usage with Wrapper

Our wrapper `infer-wrapper.py` accepts 4 arguments:

- `--programm`: path to the program under analysis (obligatory)
- `--property`: path to the property (obligatory, e.g.,`sv-comp/c/properties/unreach-call.prp`)
- `--data-model`: the data-model of the machine (optional, default: `ILP32`)
- `--version`: prints the version of the script

The script takes the inputs to build the fitting `infer` command as described [above](#raw-usage). 
In addition, it parses the output of infer to print the overall result. 
The script produces abstract violation and correctness witnesses matching the result.
The final line equals `Result:{p}` where `p in {true, false, error}` if the analysis terminates.

## Licensing

The project files, unless specified otherwise, are open source under the Apache-2.0 license.
Content in folder `lib/infer/facebook-clang-plugins/clang` is under license Apache 2.0 with LLVM exceptions,
see `lib/infer/facebook-clang-plugins/clang/LICENSE`.
Content in folders `lib/infer/facebook-clang-plugins/libtooling` and `lib/infer/infer/` is under the MIT license,
see the corresponding LICENSE files.