// This file is part of infer-sv,
// the SV-COMP wrapper for Facebook Infer (https://fbinfer.com/).
// https://gitlab.com/sosy-lab/software/infer-sv
//
// SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0
int main() { }