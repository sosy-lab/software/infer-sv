# This file is part of infer-sv,
# the SV-COMP wrapper for Facebook Infer (https://fbinfer.com/).
# https://gitlab.com/sosy-lab/software/infer-sv
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

""" Module for creating SV-COMP witnesses. """

from datetime import datetime
import hashlib
from pathlib import Path


def create_violation_witness(
    program_file: Path, property_file: Path, producer: str, architecture: str
) -> str:
    """Create a SV-COMP violation witness for the given program file and property.

     The witness will fulfill the graphml specification provided by
     https://github.com/sosy-lab/sv-witnesses .

    :param program_file: the path to the program file to create the witness for
    :param property_file: the path to the property file to create the witness for
    :param producer: the name of the tool that produced the witness
    :param architecture: the architecture the program was verified based on
    :returns: the witness content, as string
    """

    nodes_and_edges = """<node id="N0">
  <data key="entry">true</data>
</node>
<node id="N1">
  <data key="violation">true</data>
</node>
<edge id="E0" source="N0" target="N1">
  <data key="enterFunction">main</data>
  <data key="createThread">0</data>
</edge>"""
    return _get_graphml(
        program_file,
        property_file,
        producer,
        witness_type="violation_witness",
        architecture=architecture,
        nodes_and_edges=nodes_and_edges,
    )


def create_correctness_witness(
    program_file: Path, property_file: Path, producer: str, architecture: str
) -> str:
    """Create a SV-COMP correctness witness for the given program file and property.

     The witness will fulfill the graphml specification provided by
     https://github.com/sosy-lab/sv-witnesses .

    :param program_file: the path to the program file to create the witness for
    :param property_file: the path to the property file to create the witness for
    :param producer: the name of the tool that produced the witness
    :param architecture: the architecture the program was verified based on
    :returns: the witness content, as string
    """

    nodes_and_edges = """<node id="N0">
  <data key="entry">true</data>
</node>
<node id="N1"></node>
<edge id="E0" source="N0" target="N1">
  <data key="enterFunction">main</data>
  <data key="createThread">0</data>
</edge>"""
    return _get_graphml(
        program_file,
        property_file,
        producer,
        witness_type="correctness_witness",
        architecture=architecture,
        nodes_and_edges=nodes_and_edges,
    )


def _get_current_time() -> str:
    # microseconds not supported by witness format
    return datetime.now().astimezone().isoformat(timespec="seconds")


def _property_content(property_file) -> str:
    with open(property_file, encoding="UTF-8") as inp:
        return inp.read().strip()


def _get_hash(program_file: Path, hash_function=hashlib.sha256) -> str:
    hash_creator = hash_function()
    with open(program_file, "rb") as inp:
        hash_creator.update(inp.read())
    return hash_creator.hexdigest()


def _get_graphml(
    program_file,
    property_file,
    producer,
    witness_type,
    architecture,
    nodes_and_edges,
    sourcecode_lang="C",
) -> str:
    # Get time before doing anything else,
    # so that witness creation time is as close as possible
    # to the creation time of the verification result
    creationtime = _get_current_time()

    if not program_file.exists():
        raise FileNotFoundError(f"Program file {program_file} does not exist.")
    if not property_file.exists():
        raise FileNotFoundError(f"Property file {property_file} does not exist.")

    specification = _property_content(property_file)
    program_hash = _get_hash(program_file)
    return f"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 <key attr.name="originFileName" attr.type="string" for="edge" id="originfile"/>
 <key id="creationtime" attr.name="creationtime" for="graph"/>
 <key attr.name="invariant" attr.type="string" for="node" id="invariant"/>
 <key attr.name="invariant.scope" attr.type="string" for="node" id="invariant.scope"/>
 <key attr.name="namedValue" attr.type="string" for="node" id="named"/>
 <key attr.name="nodeType" attr.type="string" for="node" id="nodetype">
  <default>path</default>
 </key>
 <key attr.name="isFrontierNode" attr.type="boolean" for="node" id="frontier">
  <default>false</default>
 </key>
 <key attr.name="isViolationNode" attr.type="boolean" for="node" id="violation">
  <default>false</default>
 </key>
 <key attr.name="isEntryNode" attr.type="boolean" for="node" id="entry">
  <default>false</default>
 </key>
 <key attr.name="isSinkNode" attr.type="boolean" for="node" id="sink">
  <default>false</default>
 </key>
 <key attr.name="enterLoopHead" attr.type="boolean" for="edge" id="enterLoopHead">
  <default>false</default>
 </key>
 <key attr.name="violatedProperty" attr.type="string" for="node" id="violatedProperty"/>
 <key attr.name="threadId" attr.type="string" for="edge" id="threadId"/>
 <key attr.name="createThread" attr.type="string" for="edge" id="createThread"/>
 <key attr.name="sourcecodeLanguage" attr.type="string" for="graph" id="sourcecodelang"/>
 <key attr.name="programFile" attr.type="string" for="graph" id="programfile"/>
 <key attr.name="programHash" attr.type="string" for="graph" id="programhash"/>
 <key attr.name="specification" attr.type="string" for="graph" id="specification"/>
 <key attr.name="architecture" attr.type="string" for="graph" id="architecture"/>
 <key attr.name="producer" attr.type="string" for="graph" id="producer"/>
 <key attr.name="sourcecode" attr.type="string" for="edge" id="sourcecode"/>
 <key attr.name="startline" attr.type="int" for="edge" id="startline"/>
 <key attr.name="startoffset" attr.type="int" for="edge" id="startoffset"/>
 <key attr.name="lineColSet" attr.type="string" for="edge" id="lineCols"/>
 <key attr.name="control" attr.type="string" for="edge" id="control"/>
 <key attr.name="assumption" attr.type="string" for="edge" id="assumption"/>
 <key attr.name="assumption.resultfunction" attr.type="string" for="edge" id="assumption.resultfunction"/>
 <key attr.name="assumption.scope" attr.type="string" for="edge" id="assumption.scope"/>
 <key attr.name="enterFunction" attr.type="string" for="edge" id="enterFunction"/>
 <key attr.name="returnFromFunction" attr.type="string" for="edge" id="returnFrom"/>
 <key attr.name="predecessor" attr.type="string" for="edge" id="predecessor"/>
 <key attr.name="successor" attr.type="string" for="edge" id="successor"/>
 <key attr.name="witness-type" attr.type="string" for="graph" id="witness-type"/>
 <graph edgedefault="directed">
  <data key="creationtime">{creationtime}</data>
  <data key="witness-type">{witness_type}</data>
  <data key="sourcecodelang">{sourcecode_lang}</data>
  <data key="producer">{producer}</data>
  <data key="specification">{specification}</data>
  <data key="programfile">{program_file}</data>
  <data key="programhash">{program_hash}</data>
  <data key="architecture">{architecture}</data>
  {nodes_and_edges}
 </graph>
</graphml>
  """
