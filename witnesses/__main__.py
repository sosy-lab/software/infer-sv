# This file is part of infer-sv,
# the SV-COMP wrapper for Facebook Infer (https://fbinfer.com/).
# https://gitlab.com/sosy-lab/software/infer-sv
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
"""Main entry for witnesses"""

import argparse
from pathlib import Path
import sys
from . import create_correctness_witness, create_violation_witness


def parser():
    p = argparse.ArgumentParser()
    p.add_argument(
        "--program-file", required=True, help="Path to program file verified"
    )
    p.add_argument(
        "--prop-file", required=True, help="Path to file of property verified"
    )
    p.add_argument("--producer", required=True, help="Name of producer")
    p.add_argument(
        "--correctness",
        action="store_const",
        dest="witness_type",
        const="correctness_witness",
        help="Create a correctness witness",
    )
    p.add_argument(
        "--violation",
        action="store_const",
        dest="witness_type",
        const="violation_witness",
        help="Create a correctness witness",
    )
    p.add_argument(
        "--architecture",
        required=True,
        help="Machine architecture verification assumed",
    )
    return p


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args = parser().parse_args(argv)
    if args.witness_type == "correctness_witness":
        print(
            create_correctness_witness(
                Path(args.program_file),
                Path(args.prop_file),
                args.producer,
                args.architecture,
            )
        )
    elif args.witness_type == "violation_witness":
        print(
            create_violation_witness(
                Path(args.program_file),
                Path(args.prop_file),
                args.producer,
                args.architecture,
            )
        )
    else:
        raise ValueError("Unknown witness type")


sys.exit(main())
