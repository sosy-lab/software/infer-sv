# This file is part of frama-c-sv,
# a wrapper around Frama-C that makes it possible
# to execute Frama-C as part of the Competition on Software Verification:
# https://gitlab.com/sosy-lab/software/frama-c-svcomp
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
import re
from abc import ABC
from abc import abstractmethod
from typing import List, Tuple, Union


class Property(ABC):
    RESULT_TRUE_PROP = "true"
    RESULT_FALSE_PROP = "false"
    RESULT_UNKNOWN = "unknown"
    RESULT_ERROR = "error"
    RESULT_FALSE_OVERFLOW = RESULT_FALSE_PROP + "(no-overflow)"
    RESULT_FALSE_DEREF = RESULT_FALSE_PROP + "(valid-deref)"

    CORRECTNESS_WITNESS = "--correctness"
    VIOLATION_WITNESS = "--violation"

    @staticmethod
    def from_text(property_text):
        property_text = property_text.strip()
        if property_text == "CHECK( init(main()), LTL(G ! overflow) )":
            return NoOverflowProperty()
        if property_text == "CHECK( init(main()), LTL(G ! call(reach_error())) )":
            return UnreachProperty()
        if (
            property_text == "CHECK( init(main()), LTL(G valid-free) )"
            "\nCHECK( init(main()), LTL(G valid-deref) )"
            "\nCHECK( init(main()), LTL(G valid-memtrack) )"
        ):
            return MemorySafetyProperty()
        if "CHECK( init(main()), LTL(G ! data-race) )" in property_text:
            return DataRaceProperty()
        raise ValueError(f"unknown property {property_text}")

    @staticmethod
    def enable_options(option_names: list) -> List[str]:
        options = []
        for name in option_names:
            options.append("--enable-issue-type")
            options.append(name)
        return options

    @abstractmethod
    def get_matcher(self):
        pass

    @abstractmethod
    def get_options(self) -> List[str]:
        pass

    @staticmethod
    def found_any_issue(text: str):
        return re.search(r"Found [1-9][0-9]* issues", text)

    def parse_tool_output(
        self, output: str
    ) -> Union[Tuple[str, None], Tuple[str, str]]:
        if "Error backtrace:" in output:
            return Property.RESULT_ERROR, None
        matcher, property_group_number = self.get_matcher()
        for line in output.split("\n"):
            match = matcher.match(line)
            if match and len(match.groups()) >= property_group_number:
                if int(match[property_group_number]) > 0:
                    return Property.RESULT_FALSE_PROP, Property.VIOLATION_WITNESS
        return Property.RESULT_TRUE_PROP, Property.CORRECTNESS_WITNESS


class UnreachProperty(Property):
    def get_matcher(self):
        return re.compile(r".*reach_error_([0-9]+) = .*0x7fffffff \+ 1;.*"), 1

    def get_options(self):
        return ["--bufferoverrun"]

    def __str__(self):
        return "UnreachProperty"

    def parse_tool_output(
        self, output: str
    ) -> Union[Tuple[str, None], Tuple[str, str]]:
        if "Error backtrace:" in output or "Error: " in output:
            return Property.RESULT_ERROR, None
        matcher, property_group_number = self.get_matcher()
        overflow_matcher = re.compile(
            r".*(Integer Overflow|Buffer Overrun) ([LUS])([1-5]).*"
        )
        last_overflow_error = -1
        found_errors = set()
        found_violation = False
        for line in output.split("\n"):
            match = matcher.match(line)
            overflow_match = overflow_matcher.match(line)
            if overflow_match:
                if overflow_match[1] == "Integer Overflow" and overflow_match[2] == "L":
                    last_overflow_error = int(overflow_match[3])
                found_errors.add(
                    f"{overflow_match[1]}{overflow_match[2]}{overflow_match[3]}"
                )
            if match and len(match.groups()) >= property_group_number:
                if int(match[property_group_number]) > 0 and last_overflow_error == 1:
                    found_violation = True
        if found_violation:
            return Property.RESULT_FALSE_PROP, Property.VIOLATION_WITNESS
        if not found_errors:
            return Property.RESULT_TRUE_PROP, Property.CORRECTNESS_WITNESS
        return Property.RESULT_UNKNOWN, None


class NoOverflowProperty(Property):
    def get_matcher(self):
        return (
            re.compile(r".*(Integer Overflow|Buffer Overrun) [LUS][1-5].*: ([0-9]+)"),
            2,
        )

    def get_options(self):
        return ["--bufferoverrun"]

    def __str__(self):
        return "NoOverflowProperty"


class MemorySafetyProperty(Property):
    def get_matcher(self):
        return (
            re.compile(
                r".*\((BIABDUCTION_MEMORY_LEAK|DANGLING_POINTER_DEREFERENCE|"
                r"NULL_DEREFERENCE|RESOURCE_LEAK|USE_AFTER_DELETE|USE_AFTER_FREE|"
                r"USE_AFTER_LIFETIME|PULSE_UNINITIALIZED_VALUE|NULLPTR_DEREFERENCE|"
                r"CONSTANT_ADDRESS_DEREFERENCE|MEMORY_LEAK)\): ([0-9]+)"
            ),
            2,
        )

    def get_options(self):
        return ["--biabduction", "--pulse"]

    def __str__(self):
        return "MemorySafetyProperty"

    def parse_tool_output(
        self, output: str
    ) -> Union[Tuple[str, None], Tuple[str, str]]:
        if "Error backtrace:" in output:
            return Property.RESULT_ERROR, None
        matcher, property_group_number = self.get_matcher()
        for line in output.splitlines():
            match = matcher.match(line)
            if match and len(match.groups()) >= property_group_number:
                if int(match[property_group_number]) > 0:
                    additional_info = match[property_group_number - 1].lower()
                    if "leak" in additional_info:
                        additional_info = "(valid-memtrack)"
                    elif "dereference" in additional_info:
                        additional_info = "(valid-deref)"
                    elif "free" in additional_info:
                        additional_info = "(valid-free)"
                    return (
                        Property.RESULT_FALSE_PROP + additional_info,
                        Property.VIOLATION_WITNESS,
                    )
        return Property.RESULT_TRUE_PROP, Property.CORRECTNESS_WITNESS


class DataRaceProperty(Property):
    def get_matcher(self):
        return (
            re.compile(
                r".*(GUARDEDBY_VIOLATION|GUARDEDBY_VIOLATION_NULLSAFE|INTERFACE_NOT_THREAD_SAFE"
                r"|LOCK_CONSISTENCY_VIOLATION|THREAD_SAFETY_VIOLATION|THREAD_SAFETY_VIOLATION_NULLSAFE"
                r").*: ([0-9]+)"
            ),
            2,
        )

    def get_options(self):
        return ["--racerd"]

    def __str__(self):
        return "DataRaceProperty"
